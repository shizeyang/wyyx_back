package com.kgc.cn.config.aop.impl;

import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.config.aop.CurrentEmployee;
import com.kgc.cn.model.Employee;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Objects;

public class CurrentEmployeeMsg implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(Employee.class) &&
                parameter.hasParameterAnnotation(CurrentEmployee.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String userJsonStr = (String) webRequest.getAttribute("userJsonString", RequestAttributes.SCOPE_REQUEST);
        Employee employee = JSONObject.parseObject(userJsonStr, Employee.class);
        if (!Objects.isNull(employee)) {
            return employee;
        }
        return null;
    }
}
