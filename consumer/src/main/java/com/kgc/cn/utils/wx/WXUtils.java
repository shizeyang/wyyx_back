package com.kgc.cn.utils.wx;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Data
@Component
@ConfigurationProperties(prefix = "wx")
public class WXUtils {
    public String appid;
    public String redirect_uri;
    public String response_type;
    public String scope;
    public String secret;

    public String reqCode(String state) throws UnsupportedEncodingException {
        StringBuffer sbf = new StringBuffer("https://open.weixin.qq.com/connect/oauth2/authorize?");
        sbf.append("appid=" + appid).append("&")
                .append("redirect_uri=").append(URLEncoder.encode(redirect_uri, "utf-8")).append("&")
                .append("response_type=").append(response_type).append("&")
                .append("scope=").append(scope).append("&")
                .append("state=").append(state).append("#wechat_redirect");
        return sbf.toString();
    }

    public String reqAccessToken(String code) {
        StringBuffer sbf = new StringBuffer("https://api.weixin.qq.com/sns/oauth2/access_token?");
        sbf.append("appid=").append(appid).append("&")
                .append("secret=").append(secret).append("&")
                .append("code=").append(code).append("&")
                .append("grant_type=authorization_code");
        return sbf.toString();
    }

    public String reqUserInfo(String access_token, String openid) {
        StringBuffer sbf = new StringBuffer("https://api.weixin.qq.com/sns/userinfo?");
        sbf.append("access_token=").append(access_token).append("&")
                .append("openid=").append(openid).append("&")
                .append("lang=zh_CN");
        return sbf.toString();
    }

    public String reqRefrshToken(String refresh_token){
        StringBuffer sbf = new StringBuffer("https://api.weixin.qq.com/sns/oauth2/refresh_token?");
        sbf.append("appid=").append(appid).append("&")
                .append("grant_type=").append("refresh_token").append("&")
                .append("refresh_token=").append(refresh_token);
        return sbf.toString();
    }
}
