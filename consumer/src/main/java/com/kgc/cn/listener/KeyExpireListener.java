package com.kgc.cn.listener;

import lombok.extern.log4j.Log4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

@Log4j
public class KeyExpireListener extends KeyExpirationEventMessageListener {

    public KeyExpireListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        String msg = message.toString();
        log.info(msg);
    }
}
