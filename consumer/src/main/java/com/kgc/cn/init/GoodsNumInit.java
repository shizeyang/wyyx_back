package com.kgc.cn.init;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.model.Goods;
import com.kgc.cn.model.GoodsExample;
import com.kgc.cn.service.GoodsService;
import com.kgc.cn.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Configuration
public class GoodsNumInit implements ApplicationRunner {

    @Value("${nameSpace.goodStock}")
    private String nameSpace;

    @Autowired
    private RedisUtils redisUtils;

    @Reference
    private GoodsService goodsService;

    @Override
    @Transactional
    public void run(ApplicationArguments applicationArguments) throws Exception {
        GoodsExample goodsExample = new GoodsExample();
        List<Goods> goodsList = goodsService.getTotalGoods();
        if (CollectionUtils.isNotEmpty(goodsList)) {
            goodsList.forEach(goods -> {
                redisUtils.set(nameSpace + goods.getgId(), goods.getgStock());
            });
        }
    }
}
