package com.kgc.cn.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.config.aop.Level2Required;
import com.kgc.cn.enums.Enums;
import com.kgc.cn.service.DiscountService;
import com.kgc.cn.utils.exceI.ExcelUtils;
import com.kgc.cn.utils.returns.ReturnResult;
import com.kgc.cn.utils.returns.ReturnResultUtils;
import com.kgc.cn.vo.DiscountVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

@Api(tags = "折扣")
@RestController
@RequestMapping
public class DiscountController {

    @Reference
    private DiscountService discountService;

    @Level2Required
    @ApiOperation("增加商品折扣")
    @PostMapping(value = "/addGoodsDiscount")
    public ReturnResult<String> addGoodsDiscount(@ApiParam("折扣表") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            return ReturnResultUtils.returnFail(Enums.FileEnum.FILE_NOT_EXIST);
        }
        InputStream in = file.getInputStream();
        Map map = ExcelUtils.readExcelByModelFromInputStream(in, "discount", DiscountVo.class);
        List<DiscountVo> discountVoList = (List) map.get("discount");
        if (discountService.addGoodsDiscount(discountVoList) == 1) {
            return ReturnResultUtils.returnSuccess(Enums.GoodsEnum.ADD_DISCOUNT_SUCCESS);
        }
        return ReturnResultUtils.returnSuccess(Enums.GoodsEnum.ADD_DISCOUNT_SUCCESS);
    }

    @Level2Required
    @ApiOperation("修改商品折扣")
    @PostMapping(value = "/updateGoodsDiscount")
    public ReturnResult<String> updateGoodsDiscount(@Valid DiscountVo discountVo) {
        int flag = discountService.updateGoodsDiscount(discountVo);
        if (flag == 1) {
            return ReturnResultUtils.returnSuccess(Enums.GoodsEnum.GOODSDISCONT_UPDATE_SUCCESS);
        } else if (flag == 2) {
            return ReturnResultUtils.returnFail(Enums.GoodsEnum.NOTDISCOUNT_FAIL);
        }
        return ReturnResultUtils.returnFail(Enums.GoodsEnum.UPDATE_DISCOUNT_FAIL);
    }

}
