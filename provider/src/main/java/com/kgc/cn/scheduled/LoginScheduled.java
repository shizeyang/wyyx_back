package com.kgc.cn.scheduled;

import com.kgc.cn.mapper.AttendanceMapper;
import com.kgc.cn.mapper.DiscountMapper;
import com.kgc.cn.mapper.EmployeeMapper;
import com.kgc.cn.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class LoginScheduled {


    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private AttendanceMapper attendanceMapper;

    @Autowired
    private DiscountMapper discountMapper;

    /**
     * 员工出勤录入
     */
    @Transactional
    @Scheduled(cron = "0 0 23 1/1 * ?")
    public void loginScheduled() {
        List<Employee> employeeList = employeeMapper.showEmployeeExcel();
        employeeList.forEach(employee -> {
            attendanceMapper.insert(Attendance.toAttendance(employee));
            EmployeeExample employeeExample = new EmployeeExample();
            employeeExample.createCriteria().andEIdEqualTo(employee.geteId());
            if (employee.geteStatus() != 3) {
                employee.seteStatus(0);
                employeeMapper.updateByExample(employee, employeeExample);
            }
        });
    }

    /**
     * 定时每年员工年龄+1
     */
    @Scheduled(cron = "0 0 0 1 2 ?")
    public void ageScheduled() {
        employeeMapper.addEveryYear();
    }


    /**
     * 每天一点清理数据库中过期折扣
     */
    @Scheduled(cron = "0 0 1 * * ?")
    @Transactional
    public void delPastDiscount() {
        //搜索所有的折扣商品
        DiscountExample discountExample = new DiscountExample();
        List<Discount> discountlist = discountMapper.selectByExample(discountExample);

        //从折扣表及对应关系表中删除过期折扣
        discountlist.forEach(discount -> {
            if (discount.getdEndTime().getTime() <= System.currentTimeMillis()) {
                discountMapper.deleteByPrimaryKey(discount.getdId());
                discountMapper.delGnD(discount.getdId());
            }
        });
    }


}

