package com.kgc.cn.service;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.excel.util.ObjectUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kgc.cn.mapper.AttendanceMapper;
import com.kgc.cn.mapper.EmployeeMapper;
import com.kgc.cn.model.Attendance;
import com.kgc.cn.model.AttendanceExample;
import com.kgc.cn.model.Employee;
import com.kgc.cn.model.EmployeeExample;
import com.kgc.cn.utils.QueryId;
import com.kgc.cn.utils.activemq.ActiveMqUtils;
import com.kgc.cn.utils.aes.AesUtils;
import com.kgc.cn.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private ActiveMqUtils activeMqUtils;

    @Autowired
    private JavaMailSenderImpl javaMailSender;

    @Autowired
    private AttendanceMapper attendanceMapper;


    @Override
    public int updateEmployeeState(String id, Employee employee) {
        if (employee.geteLevel() < employeeMapper.selectByPrimaryKey(id).geteLevel()) {
            return 2;
        }
        Employee employeeDto = new Employee();
        employeeDto.seteStatus(2);
        EmployeeExample employeeExample = new EmployeeExample();
        employeeExample.createCriteria().andEIdEqualTo(id);
        if (employeeMapper.updateByExampleSelective(employeeDto, employeeExample) > 0) {
            return 1;
        }
        return 0;
    }

    @Override
    public int delEmployee(String id, Employee employee) {
        if (employee.geteLevel() < employeeMapper.selectByPrimaryKey(id).geteLevel()) {
            return 2;
        }
        if (employeeMapper.deleteByPrimaryKey(id) > 0) {
            return 1;
        }
        return 0;
    }

    @Override
    public Map<Page, List<EmployeeQueryVo>> showEmployee(EmployeeQueryVo employeeQueryVo, int pageSize,
                                                         int startRows, Employee employee) {
        Employee employeeDto = new Employee();
        BeanUtils.copyProperties(employeeQueryVo, employeeDto);
        EmployeeExample employeeExample = new EmployeeExample();
        employeeExample.setPageSize(pageSize);
        employeeExample.setStartRow(startRows);
        List<Employee> employeeList = employeeMapper.selectEmployee(employeeDto, employeeExample, employee.geteLevel());
        if (CollectionUtils.isNotEmpty(employeeList)) {
            int totalNum = employeeMapper.selectEmployeeCount(employeeDto, employee.geteLevel());
            Page page = new Page();
            page.setPage(totalNum, pageSize, startRows);
            List<EmployeeQueryVo> employeeQueryVoList = Lists.newArrayList();
            employeeList.forEach(employeeLevel -> {
                EmployeeQueryVo employeeQueryVos = employeeLevel.toEmployeeQueryVo();
                employeeQueryVoList.add(employeeQueryVos);
            });
            Map<Page, List<EmployeeQueryVo>> pageListMap = Maps.newHashMap();
            pageListMap.put(page, employeeQueryVoList);
            return pageListMap;
        }
        return null;
    }

    @Override
    public int updateEmployee(EmployeeUpdateVo employeeUpdateVo, Employee employee) {
        int level = employeeMapper.selectByPrimaryKey(employeeUpdateVo.geteId()).geteLevel();
        if (employee.geteLevel() < level) {
            return 2;
        }
        Employee employees = new Employee();
        BeanUtils.copyProperties(employeeUpdateVo, employees);
        EmployeeExample employeeExample = new EmployeeExample();
        employeeExample.createCriteria().andEIdEqualTo(employeeUpdateVo.geteId());
        if (employeeMapper.updateByExampleSelective(employees, employeeExample) > 0) {
            return 1;
        }
        return 0;
    }

    @Override
    public List<EmployeeExcelVo> showEmployeeExcelVo(int level) {
        List<Employee> employeeList = employeeMapper.showEmployeeExcel()
                .stream().filter(eachEmployee -> (eachEmployee.geteLevel()) <= level).collect(Collectors.toList());
        List<EmployeeExcelVo> employeeExcelVoList = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(employeeList)) {

            employeeList.forEach(eachEmployee -> {
                EmployeeExcelVo employeeExcelVo = Employee.employeeExcel(eachEmployee);
                employeeExcelVoList.add(employeeExcelVo);
            });
            return employeeExcelVoList;
        }
        return null;
    }

    @Override
    public int updatePassword(String id, String password) {
        if (employeeMapper.updatePassword(password, id) > 0) {
            return 1;
        }
        return 0;
    }

    @Override
    public boolean queryEmployeeById(String eid) {
        Employee employee = employeeMapper.selectByPrimaryKey(eid);
        if (ObjectUtils.isEmpty(employee)) {
            return true;
        }
        return false;
    }

    @Override
    public void vacationEmployee(String eid) {
        Employee employee = new Employee();
        employee.seteId(eid);
        employee.seteAttendance(3);
        employeeMapper.updateByPrimaryKeySelective(employee);
    }

    @Override
    public void vacationExpire(String eid) {
        Employee employee = new Employee();
        employee.seteId(eid);
        employee.seteAttendance(0);
        employeeMapper.updateByPrimaryKeySelective(employee);
    }


    @Override
    public Employee employeeLogin(EmployeeLoginVo employeeLoginVo) throws Exception {

        // 根据工号查员工
        Employee employee = employeeMapper.selectByPrimaryKey(employeeLoginVo.geteId());
        if (!ObjectUtils.isEmpty(employee)) {
            // 验证密码
            if (AesUtils.decrypt(employee.getePassword()).equals(employeeLoginVo.getePassword())) {
                if (employee.geteAttendance() == 0) {
                    int date = Integer.parseInt(new SimpleDateFormat("HHmm").format(new Date()));
                    if (date <= 900) {
                        employee.seteAttendance(1);
                    } else if (date > 900 && date <= 1000) {
                        employee.seteAttendance(2);
                    }
                    employeeMapper.updateByPrimaryKeySelective(employee);
                }
                return employee;
            }
        }
        return null;
    }


    @Override
    @Transactional
    public boolean addEmpFromExcel(List<EmployeeExcelVo> list) {
        List<Employee> employeeList = Lists.newArrayList();
        list.forEach(employeeExcel -> {
            //避开重复员工信息
            if (employeeMapper.selPhone(employeeExcel.getPhone()).isEmpty()) {

                Employee employee = employeeExcel.toEmployee();
                //生成工号
                String dId = employeeMapper.selEidFromDepartment(employee.geteDepartment());
                try {
                    employee.setePassword(AesUtils.encrypt("000000"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                employee.seteId(QueryId.queryEmployeeId(dId, employee.getePhone()));
                //存入数据库
                employeeList.add(employee);
                employeeMapper.insertSelective(employee);
            }
        });
        employeeList.forEach(eachEmployee -> {
            activeMqUtils.sendMsgByQueue("email", eachEmployee);
        });
        return true;
    }


    @JmsListener(destination = "email")
    public void listenerAndSendEmail(Employee eachEmployee) {
        sendMail(eachEmployee);
    }


    //发送邮件方法
    @Async
    public void sendMail(Employee eachEmployee) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setFrom("13889561476@163.com");
        msg.setTo(eachEmployee.geteEmail());
        msg.setSubject("入职通知");
        msg.setText("您的账号已激活\n您的工号为：" + eachEmployee.geteId() + "您的初始密码为：000000");
        javaMailSender.send(msg);
    }


    @Override
    public List<AttendanceExcelVo> showAttendToExcel(String date) {
        String[] dateStr = date.split(",");
        List<Date> dateList = Lists.newArrayList();
        Arrays.asList(dateStr).stream().forEach(dates -> {
            try {
                dateList.add(new SimpleDateFormat("yyyy/MM/dd").parse(dates));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
        AttendanceExample example = new AttendanceExample();
        example.createCriteria().andADateIn(dateList);
        List<Attendance> attendanceList = attendanceMapper.selectByExample(example);
        List<AttendanceExcelVo> attendanceExcelVo = Lists.newArrayList();
        attendanceList.forEach(attExcel -> {
            attendanceExcelVo.add(attExcel.toAttendanceExcelVo());
        });
        return attendanceExcelVo;
    }


}
