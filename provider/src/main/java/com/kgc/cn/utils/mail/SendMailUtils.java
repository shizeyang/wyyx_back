package com.kgc.cn.utils.mail;

import com.kgc.cn.config.SendMailProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Map;

@Component
public class SendMailUtils {

    @Autowired
    private SendMailProperties sendMailProperties;
    @Autowired
    private JavaMailSender javaMailSender;

    public void sendSimpleMail(String subject, String text) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(sendMailProperties.getFrom());
        mailMessage.setTo("");
        mailMessage.setSubject(subject);
        mailMessage.setText(text);
        javaMailSender.send(mailMessage);
    }

    public void sendFileMail(String subject, String text, Map<String, String> attachmentMap) throws Exception {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        // 发送的附件是否可以包含附件、html、图片等
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
        // 发件人邮箱
        messageHelper.setFrom(sendMailProperties.getFrom());
        // 收件人邮箱
        messageHelper.setTo("");
        // 标题
        messageHelper.setSubject(subject);
        // 内容
        messageHelper.setText(text);
        // 读取附件内容
        if (attachmentMap != null) {
            attachmentMap.entrySet().stream().forEach(entrySet -> {
                try {
                    // 根据文件绝对路径生成文件对象
                    File file = new File(entrySet.getValue());
                    if (file.exists()) {
                        // 将附件名和附件放入到要发送的邮件对象中
                        messageHelper.addAttachment(entrySet.getKey(), new FileSystemResource(file));
                    }
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            });
        }
        javaMailSender.send(mimeMessage);
    }


}
