package com.kgc.cn.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by boot on 2019/12/5.
 */
@Configuration
@MapperScan("com.kgc.cn.mapper")
public class MybatisConfig {
}
