package com.kgc.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "员工当日出勤情况model")
public class EmployeeAttendanceVo {

    @ApiModelProperty(value = "工号")
    private String eId;
    @ApiModelProperty(value = "姓名")
    private String eName;
    @ApiModelProperty(value = "手机号")
    private String ePhone;
    @ApiModelProperty(value = "所属部门")
    private String eDepartment;
    @ApiModelProperty(value = "当天出勤情况 0：缺席，1：正常出勤，2：迟到，3：请假")
    private Integer eAttendance;

    public String geteId() {
        return eId;
    }

    public void seteId(String eId) {
        this.eId = eId;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public String getePhone() {
        return ePhone;
    }

    public void setePhone(String ePhone) {
        this.ePhone = ePhone;
    }

    public String geteDepartment() {
        return eDepartment;
    }

    public void seteDepartment(String eDepartment) {
        this.eDepartment = eDepartment;
    }

    public Integer geteAttendance() {
        return eAttendance;
    }

    public void seteAttendance(Integer eAttendance) {
        this.eAttendance = eAttendance;
    }
}
