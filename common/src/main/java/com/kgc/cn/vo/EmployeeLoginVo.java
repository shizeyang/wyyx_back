package com.kgc.cn.vo;

import com.kgc.cn.model.Employee;
import com.kgc.cn.utils.conver.ConvertUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "员工登录用model")
public class EmployeeLoginVo implements Serializable {

    @ApiModelProperty(value = "工号", name = "eId", required = true)
    private String eId;
    @ApiModelProperty(value = "密码", name = "ePassword", required = true)
    private String ePassword;

    public String geteId() {
        return eId;
    }

    public void seteId(String eId) {
        this.eId = eId;
    }

    public String getePassword() {
        return ePassword;
    }

    public void setePassword(String ePassword) {
        this.ePassword = ePassword;
    }

    public Employee toEmployee() {
        ConvertUtil<EmployeeLoginVo, Employee> convertUtil = new ConvertUtil<>(Employee.class);
        return convertUtil.Convert(this);
    }
}
