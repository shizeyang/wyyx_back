package com.kgc.cn.service;

import com.kgc.cn.vo.DiscountVo;

import java.util.List;

public interface DiscountService {
    /**
     * 新增商品折扣信息
     *
     * @param discountVoList
     * @return
     */
    int addGoodsDiscount(List<DiscountVo> discountVoList);

    /**
     * 修改商品折扣
     *
     * @param discountVo
     * @return
     */
    int updateGoodsDiscount(DiscountVo discountVo);
}
