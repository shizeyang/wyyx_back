package com.kgc.cn.service;

import com.kgc.cn.model.Goods;
import com.kgc.cn.vo.*;

import java.util.List;
import java.util.Map;

public interface GoodsService {
    /**
     * 查询展示商品
     *
     * @param goodsQueryVo
     * @return
     */
    Map<Page, List<GoodsQueryVo>> show(GoodsQueryVo goodsQueryVo, int pageSize, int startRow);

    /**
     * 修改商品任意属性
     *
     * @param goodsUpdateVo
     * @return
     */
    boolean updateGoods(GoodsUpdateVo goodsUpdateVo);

    /**
     * 删除商品
     *
     * @param gid
     * @return
     */
    int deleteGoods(String gid);

    /**
     * 得到各类型下商品总数
     *
     * @return
     */
    Map<Page, List<GoodsTypeNumberVo>> queryTypeNumber(int pageSize, int startRow);

    /**
     * 返回商品总销量
     *
     * @return
     */
    Long querySalesVolume();


    /**
     * 通过excel表格批量添加商品到数据库
     *
     * @param list
     * @return
     */
    boolean addGoodsToSqlFromExcel(List<GoodsExcelVo> list);

    /**
     * 返回总销售额
     *
     * @return
     */
    Long queryTotalSales();

    /**
     * 查询全部商品信息
     *
     * @return
     */
    List<Goods> getTotalGoods();

}
