package com.kgc.cn.utils.conver;

import org.springframework.beans.BeanUtils;

public class ConvertUtil<D, V> {
    V v;
    public ConvertUtil(Class<V> cls){
        try {
            v = cls.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public V Convert(D d) {
        BeanUtils.copyProperties(d, v);
        return v;
    }
}
